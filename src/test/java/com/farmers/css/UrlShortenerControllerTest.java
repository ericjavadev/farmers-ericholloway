package com.farmers.css;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.ui.Model;

import com.farmers.css.controller.UrlShortenerController;
import com.farmers.css.service.UrlShortenerService;

@RunWith(SpringRunner.class)
@WebMvcTest(UrlShortenerController.class)
public class UrlShortenerControllerTest {
	 
    @Autowired
    private MockMvc mvc;
 
    @MockBean
    private UrlShortenerService mockService;
 
    private final static String mockLongUrl = "https://agents.farmers.com/search.html?_ga=2.145702644.1530164710.1533677851-920828368.1533677851";
    private final static String mockShortUrl = "http://localhost:8081/Wup99b61";
    
    @Test
    public void test_createShortUrl() throws Exception {
    	
    	//given
        when(mockService.createShortUrl(any(String.class))).thenReturn(mockShortUrl);
 
        //when
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("?url=" + mockLongUrl)).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
 
        //then
        assertEquals(mockShortUrl,result);
        assertEquals(MockHttpServletResponse.SC_CREATED, mvcResult.getResponse().getStatus());
    }
    
    @Test
    public void test_getLongUrl() throws Exception {
    	
    	//given
        when(mockService.getLongUrl(any(String.class))).thenReturn(mockLongUrl);
 
        //when
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("?url=" + mockShortUrl)).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
 
        //then
        assertEquals(mockLongUrl,result);
        assertEquals(MockHttpServletResponse.SC_OK, mvcResult.getResponse().getStatus());	
    }
    
    @Test
    public void test_deleteUrl() throws Exception {

    	//given
    	when(mockService.shortUrlExists(any(String.class))).thenReturn(true);
    	doNothing().when(mockService).deleteUrl(any(String.class));
 
        //when
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete("?url=" + mockShortUrl)).andReturn();
 
        //then
        assertEquals(MockHttpServletResponse.SC_NO_CONTENT, mvcResult.getResponse().getStatus());	
    }
    
    @Test
    public void test_getUsageCount() throws Exception {
    	
    	//given
        when(mockService.getShortUrlUsageCount(any(String.class))).thenReturn(5);
 
        //when
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/count?url=" + mockShortUrl)).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
 
        //then
        assertEquals("5",result);
        assertEquals(MockHttpServletResponse.SC_OK, mvcResult.getResponse().getStatus());	
    }
    
    @Test
    public void test_redirectShortUrl() throws Exception {

    	//given
    	when(mockService.getRedirectUrl(any(String.class))).thenReturn(mockShortUrl);
 
        //when
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(mockShortUrl)).andReturn();
               
        //then
        assertEquals(MockHttpServletResponse.SC_TEMPORARY_REDIRECT, mvcResult.getResponse().getStatus());	
    }
}	 
