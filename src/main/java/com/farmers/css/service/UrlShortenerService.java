package com.farmers.css.service;

import java.net.InetAddress;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.farmers.css.model.Url;
import com.farmers.css.repository.UrlShortenerRepository;
import com.farmers.css.util.RandomString;

@Service
public class UrlShortenerService {

	@Resource
	UrlShortenerRepository urlShortenerRepository;
	
	@Value("${server.port}")
	String serverPort;
	
	public boolean shortUrlExists(String shortUrl) {
		Url url = urlShortenerRepository.findByShortUrl(shortUrl);
		if(url == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public String createShortUrl(String longUrl) {

		RandomString randomString = new RandomString();
		String shortUrlId = "";
		boolean shortUrlIdExists = true;
		Url url = null;
		
		// make sure shortUrlId doesn't exist in database
		while(shortUrlIdExists) {
			shortUrlId = randomString.build(8);
			url = urlShortenerRepository.findByShortUrl(shortUrlId);
			if(url == null) {
				shortUrlIdExists = false;
			}
		}
		
		// increment usage count
		url = new Url();
		url.setShortUrlId(shortUrlId.toString());
		url.setLongUrl(longUrl);
		url.setUsageCount(0);
		url = urlShortenerRepository.save(url);
		
		// build short url
		String hostAddr = InetAddress.getLoopbackAddress().getHostName();
		StringBuilder builder = new StringBuilder();
		builder.append("http://").append(hostAddr).append(":").append(serverPort).append("/").append(shortUrlId);
		return builder.toString();
	}
	
	public String getShortUrl(String longUrl) {
		Url url = urlShortenerRepository.findByLongUrl(longUrl);
		return url.getShortUrlId();
	}
	
	public String getLongUrl(String shortUrlId) {
		Url url = urlShortenerRepository.findByShortUrl(shortUrlId);
		String retVal = null;
		if(url != null) {
			retVal = url.getLongUrl();
		}
		return retVal;
	}
	
	public String getRedirectUrl(String shortUrlId) {
		Url url = urlShortenerRepository.findByShortUrl(shortUrlId);
		String retVal = null;
		if(url != null) {
			int usageCount = url.getUsageCount();
			usageCount++;
			url.setUsageCount(usageCount);
			urlShortenerRepository.save(url);
			retVal = url.getLongUrl();
		}
		return retVal;
	}
	
	public Integer getShortUrlUsageCount(String shortUrlId) {
		Url url = urlShortenerRepository.findByShortUrl(shortUrlId);
		Integer retVal = null;
		if(url != null) {
			retVal = url.getUsageCount();
		}
		return retVal;
	}
	
	public void deleteUrl(String shortUrlId) {
		urlShortenerRepository.deleteByShortUrl(shortUrlId);
	}
}
