package com.farmers.css.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.farmers.css.model.Url;

@Repository
@Transactional
public interface UrlShortenerRepository extends CrudRepository<Url, Long> {
	@Query("select u from Url u where u.shortUrlId = :shortUrlId")
	Url findByShortUrl(@Param("shortUrlId") String shortUrlId);
	
	@Query("select u from Url u where u.longUrl =:longUrl")
	Url findByLongUrl(@Param("longUrl") String longUrl);
	
	@Modifying
	@Query("delete from Url u where u.shortUrlId =:shortUrlId")
	void deleteByShortUrl(@Param("shortUrlId") String shortUrlId);
}
