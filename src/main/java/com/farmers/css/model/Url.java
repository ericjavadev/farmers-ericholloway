package com.farmers.css.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "url",
		indexes = {@Index(name = "idx_short_url", columnList = "id,short_url_id", unique = true)})
public class Url {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "short_url_id", nullable = false)
	private String shortUrlId;
	
	@Column(name = "long_url", nullable = false)
	private String longUrl;
	
	@Column(name = "usage_count")
	private int usageCount;
}
