package com.farmers.css;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(basePackages = {"com.farmers.css"})
@EnableJpaRepositories(basePackages = "com.farmers.css.repository")
@EntityScan(basePackages = "com.farmers.css.model")
@EnableWebMvc
public class ApplicationConfig {

}
