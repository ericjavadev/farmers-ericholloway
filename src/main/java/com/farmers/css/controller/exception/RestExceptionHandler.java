package com.farmers.css.controller.exception;

import javax.validation.ConstraintViolationException;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.extern.slf4j.Slf4j;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	
	private ResponseEntity<Object> buildResponseEntity(UrlShortenerException urlShortenerException) {
		return new ResponseEntity<>(urlShortenerException, urlShortenerException.getStatus());
	}
	   
	// handles http requests that aren't valid
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
			  HttpRequestMethodNotSupportedException ex, 
			  HttpHeaders headers, 
			  HttpStatus status, 
			  WebRequest request) {
	    StringBuilder builder = new StringBuilder();
	    builder.append(ex.getMethod());
	    builder.append(" method is not supported for this request.");
	 
	    UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.METHOD_NOT_ALLOWED, 
	      ex.getLocalizedMessage(), builder.toString(), ex);
	    return new ResponseEntity<Object>(urlShortenerException, new HttpHeaders(), urlShortenerException.getStatus());
	}
	
	// handles invalid json string being passed in to controller
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, 
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String error = "Malformed JSON request";
		return buildResponseEntity(new UrlShortenerException(HttpStatus.BAD_REQUEST, error, ex));
	}
	
	// handles all exceptions not explicitly detailed in this class
	@ExceptionHandler(Exception.class) 
	protected ResponseEntity<Object> handleAllExceptions(Exception e, WebRequest request) {
		return buildResponseEntity(new UrlShortenerException(HttpStatus.INTERNAL_SERVER_ERROR, e));
	} 
	
	// handles validation constraits set in Entity classes
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.BAD_REQUEST, "Validation Failed", ex);
		return buildResponseEntity(urlShortenerException);
	}

	//java exception handlers below
	@ExceptionHandler(JsonProcessingException.class)
	protected ResponseEntity<Object> handleJsonProcessing(JsonProcessingException e, WebRequest request) {
		return buildResponseEntity(new UrlShortenerException(HttpStatus.INTERNAL_SERVER_ERROR, e));
	}
   
	@ExceptionHandler(IllegalArgumentException.class)
	protected ResponseEntity<Object> handleJsonProcessing(IllegalArgumentException e, WebRequest request) {
		return buildResponseEntity(new UrlShortenerException(HttpStatus.INTERNAL_SERVER_ERROR, e));
	}
	
	// ************************************************************************************************************
	// database exception handles below
	@ExceptionHandler(ConstraintViolationException.class)
	protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
		UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.CONFLICT, ex.getMessage(), ex);
		return buildResponseEntity(urlShortenerException);
	}
	
	@ExceptionHandler(TransactionSystemException.class)
	protected ResponseEntity<Object> handleTransactionSystemException(TransactionSystemException ex) {
		UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.BAD_REQUEST, "Validation Failed", ex);
		return buildResponseEntity(urlShortenerException);
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex) {
		UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.CONFLICT, ex.getMessage(), ex.getMostSpecificCause(), ex);
		return buildResponseEntity(urlShortenerException);
	}
	
	@ExceptionHandler(EmptyResultDataAccessException.class)
	protected ResponseEntity<Object> EmptyResultDataAccess(EmptyResultDataAccessException ex) {
		UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex.getMostSpecificCause(), ex);
		return buildResponseEntity(urlShortenerException);
	}
	// ************************************************************************************************************	
	
	
	// ************************************************************************************************************
    //custom exception handlers below
	@ExceptionHandler(InvalidArgumentException.class)
	protected ResponseEntity<Object> handleInvalidArgument(InvalidArgumentException ex) {
		UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
		return buildResponseEntity(urlShortenerException);
	}
	
	@ExceptionHandler(ResourceNotFoundException.class)
	protected ResponseEntity<Object> handleClientNotFoundException(ResourceNotFoundException ex) {
		UrlShortenerException urlShortenerException = new UrlShortenerException(HttpStatus.NOT_FOUND, ex.getMessage(), ex);
		return buildResponseEntity(urlShortenerException);
	}
	// ************************************************************************************************************
}
