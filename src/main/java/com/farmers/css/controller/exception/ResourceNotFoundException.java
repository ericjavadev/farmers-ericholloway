package com.farmers.css.controller.exception;

import org.springframework.http.HttpStatus;

public class ResourceNotFoundException extends UrlShortenerException {
    private static final String messageFormat = "Requested %s with id %s not found";

    public ResourceNotFoundException(String resourceName, Long resourceId) {
        super(HttpStatus.NOT_FOUND, String.format(messageFormat, resourceName, resourceId.toString()));
    }

    public ResourceNotFoundException(String resourceName, String resourceString) {
        super(HttpStatus.NOT_FOUND, String.format(messageFormat, resourceName, resourceString));
    }
}
