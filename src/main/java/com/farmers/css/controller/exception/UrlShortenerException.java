package com.farmers.css.controller.exception;

import java.time.LocalDateTime;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UrlShortenerException extends RuntimeException {

	private static final Logger log = LoggerFactory.getLogger(UrlShortenerException.class);
			
	private HttpStatus status;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	private String message;
	private String debugMessage;

   private UrlShortenerException() {
       timestamp = LocalDateTime.now();
   }

   UrlShortenerException(HttpStatus status) {
       this();
       this.status = status;
   }

   UrlShortenerException(HttpStatus status, Throwable ex) {
       this();
       this.status = status;
       this.message = "Unexpected error";
       this.debugMessage = ex.getLocalizedMessage();
       log.error("error ->", ex);
   }
   
   UrlShortenerException(HttpStatus status, String message, Throwable ex) {
       this();
       this.status = status;
       this.message = message;
       this.debugMessage = ExceptionUtils.getRootCauseMessage(ex);
       log.error("error ->", ex);
   }

   UrlShortenerException(HttpStatus status, String debugMessage) {
       this();
       this.status = status;
       this.message = debugMessage;
//       this.debugMessage = debugMessage;
//       log.error("error ->", ex);
   }
   
   UrlShortenerException(HttpStatus status, String message, String debugMessage, Throwable ex) {
       this();
       this.status = status;
       this.message = message;
       this.debugMessage = debugMessage;
       log.error("error ->", ex);
   }
   
   UrlShortenerException(HttpStatus status, String message, Throwable debugMessage, Throwable ex) {
       this();
       this.status = status;
       this.message = message;
       this.debugMessage = debugMessage.toString();
       log.error("error ->", ex);
   }
   
   @JsonIgnore
   public StackTraceElement[] getStackTrace() {
       return super.getStackTrace();
   }

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDebugMessage() {
		return debugMessage;
	}

	public void setDebugMessage(String debugMessage) {
		this.debugMessage = debugMessage;
	}
	
	
}