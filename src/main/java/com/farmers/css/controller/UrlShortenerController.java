package com.farmers.css.controller;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.farmers.css.controller.exception.ResourceNotFoundException;
import com.farmers.css.service.UrlShortenerService;

@RestController
@RequestMapping("/")
public class UrlShortenerController {

	@Resource
	UrlShortenerService urlShortenerService;
	
	@Value("${server.servlet.contextPath}")
	String contextPath;
	
	// health check
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/health-check")
	public String healthCheck() {
		return "OK";
	}
	
	// Create a shortened URL from a long URL
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public String createShortUrl(@RequestParam(value = "url", required = true) String longUrl) {
		String shortUrl = urlShortenerService.createShortUrl(longUrl);
		return shortUrl;
	}
	
	// Retrieve the original URL from a shortened URL
	@ResponseStatus(HttpStatus.OK)
	@GetMapping
	public String getLongUrl(@RequestParam(value = "url", required = true) String shortUrl) {
		String shortUrlId = parseShortUrlId(shortUrl);
		String longUrl = urlShortenerService.getLongUrl(shortUrlId);
		if(longUrl == null) {
			throw new ResourceNotFoundException("resource not found", shortUrl);
		}
		return longUrl;
	}
    
	// Delete a shortened URL
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping
    public void deleteUrl(@RequestParam(value = "url", required = true) String shortUrl) {
    	String shortUrlId = parseShortUrlId(shortUrl);
    	if(!urlShortenerService.shortUrlExists(shortUrlId)) {
    		throw new ResourceNotFoundException("resource not found", shortUrl);
    	}
    	urlShortenerService.deleteUrl(shortUrlId);
    }
    
    // Retrieve a count of the number of times that a shortened URL has been used
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/count")
    public int getUsageCount(@RequestParam(value = "url", required = true) String shortUrl) {
    	String shortUrlId = parseShortUrlId(shortUrl);
    	Integer usageCount = urlShortenerService.getShortUrlUsageCount(shortUrlId);
    	if(usageCount == null) {
			throw new ResourceNotFoundException("resource not found", shortUrl);
		}
    	return usageCount.intValue();
    }
    
	// Redirect a user to the long URL when they access the short URL from your API
	@ResponseStatus(HttpStatus.TEMPORARY_REDIRECT)
	@GetMapping("{shortUrlId}")
	public RedirectView redirectShortUrl(@PathVariable(value = "shortUrlId", required = true) String shortUrlId) {
		String longUrl = urlShortenerService.getRedirectUrl(shortUrlId);
		if(longUrl == null) {
			throw new ResourceNotFoundException("resource not found", shortUrlId);
		}
		return new RedirectView(longUrl);
	}
	
	private String parseShortUrlId(String shortUrl) {
		int totalLength = shortUrl.length();
		int lastIndex = shortUrl.lastIndexOf("/") + 1;
		return shortUrl.substring(lastIndex, totalLength);
	}
}
